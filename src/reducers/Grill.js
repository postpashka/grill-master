import { 
  SET_INIT_GRILL_ITEMS,
  SET_GRILL_OPTIONS,
  SET_GRILL_OUTPUT_FIT,
  SET_GRILL_OUTPUT_DONT_FIT,
} from "../actions/types";


const initialState = {
  initGrillItems: {},
  options: {},
  outputFit: [],
  outputDontFit: [],
};

export default function assetsReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {

    case SET_INIT_GRILL_ITEMS:
      return {
          ...state,
          initGrillItems: payload.grillItems
      };

    case SET_GRILL_OPTIONS:
      return {
          ...state,
          options: payload.options
      };

    case SET_GRILL_OUTPUT_FIT:
      return {
          ...state,
          outputFit: payload.outputFit
      };

    case SET_GRILL_OUTPUT_DONT_FIT:
      return {
          ...state,
          outputDontFit: payload.outputDontFit
      };

    default:
      return state;
  }
}