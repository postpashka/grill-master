
import React, { Fragment, useState, useEffect } from 'react';

import { useDispatch, useSelector } from "react-redux";

import Packer from '../libs/Packer';

import Home from '../pages/Home';

import { setGrillItems } from '../actions/grill';

import { 
	Container,
	Row, 
	Col,
	Input,
	Button,
	Alert,
	Card,
	CardHeader,
	Table,
	UncontrolledPopover,
	PopoverHeader,
	PopoverBody
} from 'reactstrap';


function GrillPlane( props ) {

	return (
		<div
			style={{
				overflow: 'auto',
			}}
		>

			<div 
				className="position-relative d-block m-auto bg-light"
				style={{
					width: props.options.width+'px',
					height: props.options.height+'px'
				}}
			>

			{
				props.outputArray.map(( item, index) =>
					<Fragment key={index}>
						<div 
							id={'item'+index}
							className="position-absolute bg-success"
							style={{
								height: item.height+'px',
								width: item.width+'px',
								top: item.top+'px',
								left: item.left+'px',
								border: item.borderWidth+'px solid lightblue',
							}}
						>
						</div>
						<UncontrolledPopover trigger="hover" placement="top" target={'item'+index}>
						   <PopoverBody>{item.title}</PopoverBody>
						 </UncontrolledPopover>
					</Fragment>
				)

			}
			</div>
		</div>
	)
	  

}

function OutOfGrill( props ) {

	return (
		<Card>
			<CardHeader>Items out of grid</CardHeader>

			{
				(props.outputArray.length==0) && (
					<div className="text-center" >none</div>
				)
			}

			{
				(props.outputArray.length!=0) && (
					<Table className="mb-0">
						<thead>
							<tr>
								<th>Type</th>
								<th>Size</th>
								<th>Count</th>
							</tr>
						</thead>
						<tbody>
						{			
							props.outputArray.map(( item, index) =>

								<tr key={index}>
								  <th scope="row">{item.title}</th>
								  <td>{item.h}x{item.w}</td>
								  <td>{item.count}</td>
								</tr>
							)
						}

					  </tbody>
					</Table>
				)
			}

		</Card>
	);
}

function GrillInputs() {


	let defaultInputValue = `{
	  "grill": {
		"width": 400,
		"height": 200,
		"grillItems": [{
		  "width": 50,
		  "height": 30,
		  "count": 15,
		  "title": "Steak"
		}, {
		  "width": 140,
		  "height": 140,
		  "count": 2,
		  "title": "Sausage"
		}, {
		  "width": 130,
		  "height": 60,
		  "count": 4,
		  "title": "Tomato"
		}, {
		  "width": 20,
		  "height": 10,
		  "count": 37,
		  "title": "Veal"
		}]
	  }
	}`;


	const dispatch = useDispatch();

	const [inputValue, setInputValue] = useState(defaultInputValue);
	const [isError, setIsError] = useState('');

	function IsJsonString(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return e.message
		}
		return '';
	}


	function onSubmit(){

		let errorsString = IsJsonString(inputValue);

		setIsError(errorsString);

		(!errorsString) && dispatch( setGrillItems( JSON.parse(inputValue) ) )

	}


	return (
		<div className="py-3">
			<Input 
				style={{resize: 'none'}}
				rows="10"
				type="textarea" 
				name="text" 
				value={inputValue}
				onChange={(e)=>setInputValue(e.target.value)}

			/>
			{
				!!isError && (			
					<Alert 
						color="warning"
					>
					  {isError}
					</Alert>
				)
			}
			<div className="text-right p-2">
				<Button 
					color="secondary"
					onClick={()=>onSubmit()}
				>
					redraw
				</Button>
			</div>
		</div>
	);
}

function Grill() {

	const initGrillItems = useSelector(state => state.Grill.initGrillItems)
	const options = useSelector(state => state.Grill.options)
	const outputFit = useSelector(state => state.Grill.outputFit)
	const outputDontFit = useSelector(state => state.Grill.outputDontFit)

  return (
	<div className="Grill">

		<header
			className="p-3 bg-light color-gray"
		>
			Grillmaster
		</header>
		<Container>
			<Row>

				<Col sm={8}>
					<GrillPlane 
						outputArray={ outputFit }
						options={ options } 
					/>
					<GrillInputs/>
				</Col>
				<Col sm={4}>
					<OutOfGrill
						outputArray={ outputDontFit }
					/>
				</Col>
			</Row>
		</Container>



	</div>
  );
}

export default Grill;

