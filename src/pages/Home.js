
import Grill from '../components/Grill';


function Home() {
  return (
    <div className="home">
      <Grill/>
    </div>
  );
}

export default Home;
