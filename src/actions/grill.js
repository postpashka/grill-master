import { 
  SET_GRILL_ITEMS,
  SET_INIT_GRILL_ITEMS,
  SET_GRILL_OPTIONS,
  SET_GRILL_OUTPUT_FIT,
  SET_GRILL_OUTPUT_DONT_FIT,
} from "./types";

import Packer from '../libs/Packer';

import _ from 'underscore';

export const setInitGrillItems = grillItems => ({
  type: SET_INIT_GRILL_ITEMS,
  payload: {
    grillItems
  }
});

export const setGrillOptions = options => ({
  type: SET_GRILL_OPTIONS,
  payload: {
    options
  }
});

export const setGrillOutputFit = outputFit => ({
  type: SET_GRILL_OUTPUT_FIT,
  payload: {
    outputFit
  }
});

export const setGrillOutputDontFit = outputDontFit => ({
  type: SET_GRILL_OUTPUT_DONT_FIT,
  payload: {
    outputDontFit
  }
});

export const setGrillItems = (grillItems) => {
  return dispatch => {

    dispatch(setInitGrillItems(grillItems));

    let options = {
      width: grillItems.grill.width,
      height: grillItems.grill.height
    }

    dispatch(setGrillOptions( options ));


    let formattedBlocks = getFormattedBlocks(grillItems)
    
    let output = redraw(options, formattedBlocks)


    dispatch(setGrillOutputFit( output.fit ));
    dispatch(setGrillOutputDontFit( convertWithCount(output.dontFit) ));




  };
};

function getFormattedBlocks(grillItems){
  let formattedBlocks = []

  grillItems.grill.grillItems.map(function(item) {

    if (item.count) {
      for (var i = 0; i < item.count; i++) {
        formattedBlocks.push(
          {
            w: item.width,
            h: item.height,
            title: item.title,
          }
        )
      }
    }
  });
  return formattedBlocks
}


function redraw(options, blocks){

  var output = {
    fit: [],
    dontFit: []
  }

  var blockCount  = 100;
  
  var binWidth   = options.width;
  var binHeight  = options.height;

  var maxHeight  = binHeight;
  var maxWidth   = binWidth;
  var min  = 20;
  
  // Instantiate Packer
  var packer = new Packer( binWidth, binHeight);
  
  // Sort inputs by area for best results.
  blocks = blocks.sort(function(a,b) { return (b.w*b.h < a.w*a.h ); }); 
  packer.fit(blocks);

  // Draw the blocks
  _.each(blocks, function(o,i){

    var block = blocks[i];
  
    if (block.fit) {
      var borderWidth = 2;
      var h = block.h - (borderWidth*4);
      var w = block.w - (borderWidth*4);
      var cssClass = ['fadeInLeftBig','fadeInRightBig'][_.random(0,1)];


      var item = {
        height: h, 
        width: w, 
        top: block.fit.y, 
        left: block.fit.x, 
        borderWidth: borderWidth,
        title: block.title
      }
      output.fit.push(item);
    } else {

      output.dontFit.push(block);
    }
  
  });

  return output;

}


const convertWithCount = (arr) => {
  const res = {};
  arr.forEach((obj) => {
     const key = `${obj.w}${obj.h}${obj.title}`;
     if (!res[key]) {
        res[key] = { ...obj, count: 0 };
     };
     res[key].count += 1;
  });
  return Object.values(res);
};